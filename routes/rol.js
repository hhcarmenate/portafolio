var express = require('express');
var router = express.Router();
var RolController = require('../controllers/RolController');

/* GET users listing. */

router.post('/create', RolController.createRol );

module.exports = router;
