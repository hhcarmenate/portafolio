'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RolSchema = Schema({
    name: String,
    alias: String,
 });

module.exports = mongoose.model('Rol', RolSchema);