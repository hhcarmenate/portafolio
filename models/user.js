'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    name: String,
    username: String,
    password: String,
    email: String,
    notifications: String,
    image: String,
    roles: { type: Schema.ObjectId, ref: 'rol'}
});

module.exports = mongoose.model('User', UserSchema);