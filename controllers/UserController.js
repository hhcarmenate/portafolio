/**
 * Created by hhcarmenate on 5/27/2017.
 */
'use strict'

var fs = require('fs');
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var jwt = require('../services/jwtService');

/**
 * Create new user on the system
 */
function createUser(req,resp){
    var params = req.body;
    var user = new User;

    if(!params.name || !params.username || !params.password || !params.email ){
        return resp.status(500).send({message: 'Some params that are required are missed, please fill it correctly and try again'})
    }

    bcrypt.hash(params.password, null, null, function(err,hash){
        user.password = hash;
        user.name = params.name;
        user.username = params.name;
        user.email = params.name;
        user.notifications = params.notifications;
        user.image = null;
        user.roles = 'ROLE_USER';

        user.save((err,user_saved) => {
            if(err){
                console.log(err);
                return resp.status(500).json({message: 'Error saving the user, please try again'});
            } else {
                if(!user_saved){
                    return resp.status(404).json({message: 'Error recording this user in the database, please fixed all error and try again'});
                } else {
                    return resp.status(200).json({message: 'User registered successfully', user:user_saved}); 
                }
            }
        });

    });
}

module.exports = {
    createUser,
};