'use strict'

var Rol =  require('../models/rol');

/**
 * Saving new rol in the database
 */
function createRol(req,resp){
    var params =  req.body;
    var rol = new Rol;
    console.log('testing');
    if(!params.name || !params.alias){
        return resp.status(500).json({message: 'Some params that are required are missing, please fill it correctly'});
    } 

    rol.alias = params.alias;
    rol.name = params.name;

    rol.save((err, rol_save) => {
        if(err){
            return resp.status(500).json({ message: 'Error conecting this role in the database, please fix it and try again'});
        } else {
            if(!rol_save){
                return resp.status(404).json({ message: 'Error saving this role in the database, please fix it and try again'});
            } else {
                return resp.status(200).json({ message: 'Rol was saved successfully', rol: rol_save});
            }
        }
    });


}

module.exports = {
    createRol,
};